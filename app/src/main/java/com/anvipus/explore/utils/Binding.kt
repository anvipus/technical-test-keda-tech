package com.anvipus.explore.utils

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.anvipus.library.util.toCurrency

@BindingAdapter("app:amount_text")
fun amount(textView: TextView, value: Double?) {
    textView.text = value?.toCurrency()
}