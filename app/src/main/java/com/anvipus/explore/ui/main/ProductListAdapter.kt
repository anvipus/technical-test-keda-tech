package com.anvipus.explore.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.anvipus.explore.R
import com.anvipus.explore.databinding.ItemProductBinding
import com.anvipus.explore.utils.loadImageFromAsset
import com.anvipus.library.model.Product
import com.anvipus.library.util.Helpers
import com.anvipus.library.util.state.AccountManager
import okhttp3.internal.notify

class ProductListAdapter(private val itemClickCallback: (Int) -> Unit): ListAdapter<Product, RecyclerView.ViewHolder>(COMPARATOR){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return ViewHolder(ItemProductBinding.bind(view))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = getItem(position)
        (holder as ViewHolder).run {
            bind(data,position, itemClickCallback)

        }
    }
    class ViewHolder(private val binding: ItemProductBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(data: Product?,position: Int,itemClickCallback: (Int) -> Unit){
            binding.data = data
            if(data!!.isFavourite){
                binding.ivIconFav.loadImageFromAsset(R.drawable.ic_favorit_red)
            }else{
                binding.ivIconFav.loadImageFromAsset(R.drawable.ic_favorit_gray)
            }

            binding.ivIconFav.setOnClickListener {
                itemClickCallback.invoke(position)
            }
        }
    }

    companion object{
        const val TAG:String = "ProductListAdapter"
        val COMPARATOR = object : DiffUtil.ItemCallback<Product>(){
            override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
                return oldItem.productName == newItem.productName
            }

        }
    }
}