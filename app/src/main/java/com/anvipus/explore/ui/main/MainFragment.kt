package com.anvipus.explore.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.activityViewModels
import com.anvipus.explore.R
import com.anvipus.explore.base.BaseFragment
import com.anvipus.explore.databinding.MainFragmentBinding
import com.anvipus.explore.utils.linear
import com.anvipus.library.model.Product
import com.anvipus.library.util.Helpers
import com.anvipus.library.util.state.AccountManager
import com.bumptech.glide.Glide
import com.codedisruptors.dabestofme.di.Injectable
import com.google.rpc.Help
import java.util.Locale
import javax.inject.Inject

class MainFragment : BaseFragment(), Injectable {

    companion object {
        fun newInstance() = MainFragment()
    }

    override val layoutResource: Int
        get() = R.layout.main_fragment

    override val statusBarColor: Int
        get() = R.color.colorAccent


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModelMain: MainViewModel by activityViewModels { viewModelFactory }

    @Inject
    lateinit var am: AccountManager

    private lateinit var binding: MainFragmentBinding
    private var isProductName = true
    private var isFavorit: Boolean? = false

    private var mAdapter = ProductListAdapter { position ->
       setFavorit(position)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ownTitle("")
        ownIcon(null)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = MainFragmentBinding.bind(view)
        initView()
        initViewAction()
    }

    private fun initView(){
        with(binding){
            with(viewModelMain){
                if(Helpers.generateProducts(am).isNullOrEmpty().not()){
                    am.listOfProduct = Helpers.generateProducts(am)!!
                }
                with(rvMain){
                    linear()
                    adapter = mAdapter
                }
                mAdapter.submitList(am.listOfProduct)

                val selectedRadioButtonId: Int = radioGroup.checkedRadioButtonId
                if (selectedRadioButtonId != -1) {
                    val radio: RadioButton = requireView().findViewById(selectedRadioButtonId)
                    val string = radio.text.toString()
                    if(string.contains("ya",ignoreCase = true)){
                        isFavorit = true
                    }else{
                        isFavorit = false
                    }
                }
            }
        }
    }

    private fun initViewAction(){
        with(binding) {
            etSearch.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(s: Editable?) {
                    filterList(s, am.listOfProduct,isFavorit)
                }
            })
            cardView1.setOnClickListener {
                isProductName = true
                cardView1.background = resources.getDrawable(R.drawable.border_drawable_line_blue_2)
                cardView2.background = resources.getDrawable(R.drawable.border_drawable_background)

            }
            cardView2.setOnClickListener {
                isProductName = false
                cardView1.background = resources.getDrawable(R.drawable.border_drawable_background)
                cardView2.background = resources.getDrawable(R.drawable.border_drawable_line_blue_2)

            }
            radioLeft.setOnClickListener {
                isFavorit = null
                mAdapter.submitList(am.listOfProduct)
            }
            radioCenter.setOnClickListener {
                isFavorit = true
                if(etSearch.text.toString().isEmpty().not()){
                    filterList(etSearch.text.toString(),am.listOfProduct,isFavorit!!)
                }else{
                    val list = mutableListOf<Product>()
                    val itemFilter: MutableList<Product> = ArrayList()
                    itemFilter.addAll(am.listOfProduct)
                    list.addAll(itemFilter.filter {
                        it.isFavourite == true
                    })
                    mAdapter.submitList(list)
                }

            }
            radioRight.setOnClickListener {
                isFavorit = false
                if(etSearch.text.toString().isEmpty().not()){
                    filterList(etSearch.text.toString(),am.listOfProduct,isFavorit!!)
                }else{
                    val list = mutableListOf<Product>()
                    val itemFilter: MutableList<Product> = ArrayList()
                    itemFilter.addAll(am.listOfProduct)
                    list.addAll(itemFilter.filter {
                        it.isFavourite == false
                    })
                    mAdapter.submitList(list)
                }

            }
        }
    }

    fun filterList(query: CharSequence?, itemList: MutableList<Product>, isFavorit: Boolean?) {
        val list = mutableListOf<Product>()
        if(isFavorit != null && isFavorit){
            if (query.isNullOrEmpty().not() && isProductName) {
                list.addAll(itemList.filter {
                    it.productName?.lowercase(Locale.getDefault())!!.contains(query.toString().lowercase(
                        Locale.getDefault())) && it.isFavourite
                })
            } else if(query.isNullOrEmpty().not() && !isProductName){
                list.addAll(itemList.filter {
                    it.productDescription?.lowercase(Locale.getDefault())!!.contains(query.toString().lowercase(
                        Locale.getDefault())) && it.isFavourite
                })
            }
            else {
                list.addAll(itemList)
            }
        }else if(isFavorit != null && !isFavorit){
            if (query.isNullOrEmpty().not() && isProductName) {
                list.addAll(itemList.filter {
                    it.productName?.lowercase(Locale.getDefault())!!.contains(query.toString().lowercase(
                        Locale.getDefault())) && !it.isFavourite
                })
            } else if(query.isNullOrEmpty().not() && !isProductName){
                list.addAll(itemList.filter {
                    it.productDescription?.lowercase(Locale.getDefault())!!.contains(query.toString().lowercase(
                        Locale.getDefault())) && !it.isFavourite
                })
            }
            else {
                list.addAll(itemList)
            }
        }else{
            if (query.isNullOrEmpty().not() && isProductName) {
                list.addAll(itemList.filter {
                    it.productName?.lowercase(Locale.getDefault())!!.contains(query.toString().lowercase(
                        Locale.getDefault()))
                })
            } else if(query.isNullOrEmpty().not() && !isProductName){
                list.addAll(itemList.filter {
                    it.productDescription?.lowercase(Locale.getDefault())!!.contains(query.toString().lowercase(
                        Locale.getDefault()))
                })
            }
            else {
                list.addAll(itemList)
            }
        }

        mAdapter.submitList(list)
    }

    fun setFavorit(position: Int){
        val data = am.listOfProduct[position]
        if(data.isFavourite){
            Helpers.setProductFavourite(isFavourite = false,position,am,null)
        }else{
            Helpers.setProductFavourite(isFavourite = true,position,am,null)
        }
        Helpers.saveFavouriteProduct(am)
        mAdapter.notifyDataSetChanged()
    }

}