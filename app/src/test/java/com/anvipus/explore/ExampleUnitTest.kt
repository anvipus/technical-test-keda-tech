package com.anvipus.explore

import com.anvipus.library.util.Helpers
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun generateDataProductSuccess() {
        val result = Helpers.generateProducts(null)
        assertEquals(20,result?.size)
    }

    @Test
    fun setFavoriteSuccess() {
        val listData = Helpers.generateProducts(null)
        val result = Helpers.setProductFavourite(isFavourite = true,1,null,listData)
        assertEquals(true,result)
    }
}