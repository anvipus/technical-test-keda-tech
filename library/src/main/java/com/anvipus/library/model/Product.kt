package com.anvipus.library.model

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize


@JsonClass(generateAdapter = true)
@Parcelize
class Product (
    @Json(name = "id")
    val id: Int?,
    @Json(name = "product_name")
    val productName: String?,
    @Json(name = "product_description")
    val productDescription: String?,
    @Json(name = "stock")
    val stock: Int?,
    @Json(name = "price")
    val price: Double?,
    @Json(name = "is_favourite")
    var isFavourite: Boolean = false,
) : Parcelable